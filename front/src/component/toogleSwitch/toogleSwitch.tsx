import React from "react";
import "../toogleSwitch/toogleSwtich.css";

type toogleProps = {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
};

export const ToggleSwitch = ({ onChange }: toogleProps): JSX.Element => {
  return (
    <div className="containerToogle">
      <div className="checkbox">
        <input onChange={onChange} type="checkbox" name="switch" id="switch" />
        <div className="checkbox-inner">
          <label htmlFor="switch"></label>
          <span></span>
        </div>
        <div className="checkbox__on">Arrivée</div>
        <div className="checkbox__off">Départ</div>
      </div>
    </div>
  );
};
