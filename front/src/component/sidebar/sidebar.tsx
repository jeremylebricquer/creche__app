import React, { MouseEventHandler } from "react";
import "../sidebar/sidebar.css";
import {
  RiMenuLine,
  RiLayoutGridFill,
  RiRestaurantFill,
  RiMoonClearFill,
} from "react-icons/ri";
import { FiClock } from "react-icons/fi";
import { FaHandScissors, FaBaby } from "react-icons/fa";
import { GiHealthNormal } from "react-icons/gi";
import { MdPhotoCamera } from "react-icons/md";
import { CgNotes } from "react-icons/cg";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BrowserRouter as Route, NavLink } from "react-router-dom";

export const Sidebar = (props: {
  isExpand: boolean;
  toggle: MouseEventHandler;
}): JSX.Element => {
  return (
    <div className={props.isExpand ? "sidebar" : "sidebar collapsed"}>
      <div className="sidebar-header">
        <RiMenuLine className="sidebar-icon" onClick={props.toggle} />
        <h1 className="sidebar-menu">Menu</h1>
      </div>
      <div className="sidebar-items">
        <NavLink exact to="/" className="item">
          <RiLayoutGridFill className="sidebar-icon" />
          <span className="sidebar-text">Accueil</span>
        </NavLink>

        <NavLink exact to="/pointing" className="item">
          <FiClock className="sidebar-icon" />
          <span className="sidebar-text">Pointage</span>
        </NavLink>

        <NavLink exact to="/food" className="item">
          <RiRestaurantFill className="sidebar-icon" />
          <span className="sidebar-text">Repas</span>
        </NavLink>

        <NavLink exact to="/activity" className="item">
          <FaHandScissors className="sidebar-icon" />
          <span className="sidebar-text">Activité</span>
        </NavLink>

        <NavLink exact to="/sleep" className="item">
          <RiMoonClearFill className="sidebar-icon" />
          <span className="sidebar-text">Sieste</span>
        </NavLink>

        <NavLink exact to="/health" className="item">
          <GiHealthNormal className="sidebar-icon" />
          <span className="sidebar-text">Santé</span>
        </NavLink>

        <NavLink exact to="/hygiene" className="item">
          <FaBaby className="sidebar-icon" />
          <span className="sidebar-text">Hygiène</span>
        </NavLink>

        <NavLink exact to="/photos" className="item">
          <MdPhotoCamera className="sidebar-icon" />
          <span className="sidebar-text">Photos</span>
        </NavLink>

        <NavLink exact to="/transmission" className="item">
          <CgNotes className="sidebar-icon" />
          <span className="sidebar-text">Transmission</span>
        </NavLink>
      </div>
    </div>
  );
};
