import React, { useState } from "react";
import { useFormContext } from "react-hook-form";
import "../timePicker/timePicker.css";

type TimePickerProps = {
  name: string;
  label: string;
  required: boolean;
  min: string;
  max: string;
};

/** 
  usestate pour récupérer la valeur de l'input
  input = event.target
  output = time 
  */
export const TimePicker = ({
  name,
  label,
  required,
  min,
  max,
}: TimePickerProps): JSX.Element => {
  getTime();
  const [time, setTime] = useState<string>();
  const onChanged = (e: { target: { value: string } }) => {
    const value = e.target.value;
    setTime(value);
  };
  const { register } = useFormContext();
  return (
    <>
      <div className="col-3">
        <label htmlFor={name}>{label} </label>
      </div>
      <div className="col-auto">
        <input
          type="time"
          id="time"
          min={min}
          max={max}
          required={required}
          {...register("timepicker")}
          onChange={onChanged}
        />
      </div>
    </>
  );
};

function getTime() {
  const time = new Date();
  const hour = time.getHours();
  const min = time.getMinutes();
  const completeTime = hour + ":" + min;
  return completeTime;
}
