/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from "react";
import { RiEdit2Line } from "react-icons/ri";
import "../button/btn.css";

export const EditButton = ({ parentShowFn }: any): JSX.Element => {
  const handleClick: React.MouseEventHandler<SVGElement> = () => {
    parentShowFn();
  };

  return (
    <div className="col-auto">
      <div className="row align-items-center">
        <RiEdit2Line onClick={handleClick} className="btn-icon"></RiEdit2Line>
      </div>
    </div>
  );
};
