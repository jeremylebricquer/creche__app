import React, { useState } from "react";
import { Sidebar } from "./sidebar/sidebar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "../view/home";
import { Statuts } from "../states/statuts";
import { Health } from "../states/health";
import { Hygiene } from "../states/hygiene";
import { Food } from "../states/food";
import { Sleep } from "../states/sleep";
import { Photos } from "../states/photo";
import { Transmissions } from "../states/transmissions";
import { Activity } from "../states/activity";

export const Index = (): JSX.Element => {
  const sidebarCollapsed = localStorage.getItem("sidebar-collapsed");
  const [isExpanded, setIsExpanded] = useState(sidebarCollapsed ? false : true);
  const handleToggler = () => {
    if (isExpanded) {
      setIsExpanded(false);
      -localStorage.setItem("sidebar-collapsed", "true");
      return;
    }
    setIsExpanded(true);
    localStorage.removeItem("sidebar-collapsed");
  };
  /*   console.log("expand", isExpanded); */
  return (
    <div className="container">
      <div className="row">
        <Router>
          <div className="col">
            <Sidebar isExpand={isExpanded} toggle={handleToggler} />
          </div>
          <div
            className={
              isExpanded
                ? "content col-md-10 offset-md-2"
                : "content col-10 offset-md-1 "
            }
          >
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/pointing" component={Statuts} />
              <Route exact path="/food" component={Food} />
              <Route exact path="/activity" component={Activity} />
              <Route exact path="/sleep" component={Sleep} />
              <Route exact path="/health" component={Health} />
              <Route exact path="/hygiene" component={Hygiene} />
              <Route exact path="/photos" component={Photos} />
              <Route exact path="/transmission" component={Transmissions} />
            </Switch>
          </div>
        </Router>
      </div>
    </div>
  );
};
