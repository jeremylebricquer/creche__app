import React from "react";
import { useFormContext } from "react-hook-form";
import "../observation/observation.css";

type ObvProps = {
  name: string;
  label: string;
  rows: number;
  cols: number;
};

export const Observation = ({
  name,
  label,
  rows,
  cols,
}: ObvProps): JSX.Element => {
  const { register } = useFormContext();
  return (
    <>
      <label htmlFor={name}>{label}</label>
      <textarea {...register(`${name}`)} name={name} rows={rows} cols={cols} />
    </>
  );
};
