import React from "react";
import { useFormContext } from "react-hook-form";
import "../sleepReview/sleepReview.css";

type SleepProps = {
  name: string;
  label: string;
};

export const SleepReview: React.FC<SleepProps> = ({
  label,
  name,
}: SleepProps) => {
  const { register } = useFormContext();

  return (
    <>
      <div className="col-3">
        <label htmlFor={name}>{label}</label>
      </div>
      <div className="col-auto">
        <ul className="flex-container longhand">
          <li className="flex-item">
            <input
              className="medium"
              type="radio"
              value="Aucune"
              {...register("sleep")}
            />
            <label className="text">Aucune</label>
          </li>

          <li className="flex-item">
            <input
              className="medium"
              type="radio"
              value="Courte"
              {...register("sleep")}
            />
            <label className="text">Courte</label>
          </li>

          <li className="flex-item">
            <input
              className="medium"
              type="radio"
              value="Agitée"
              {...register("sleep")}
            />
            <label className="text">Agitée</label>
          </li>
          <li className="flex-item">
            <input
              className="medium"
              type="radio"
              value="Bonne"
              {...register("sleep")}
            />
            <label className="text">Bonne</label>
          </li>
        </ul>
      </div>
    </>
  );
};
