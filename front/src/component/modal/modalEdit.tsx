import React from "react";
import { Modal } from "react-bootstrap";
import { Chld } from "../../Interfaces/Children";
import { Presence } from "../../Interfaces/Presence";
import { SimpleTabs } from "../../view/formTab";
import "./modal.css";

type EditProps = {
  show: boolean;
  onHide: React.MouseEventHandler<HTMLButtonElement>;
  id: number;
  child: Chld[];
  pres: Presence[];
};

export const ModalEdit = (props: EditProps): JSX.Element => {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      animation={false}
      keyboard={true}
    >
      <Modal.Header
        closeButton
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Modal.Title id="contained-modal-title-vcenter ">
          Modifier le pointage
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <SimpleTabs {...props} />
      </Modal.Body>
    </Modal>
  );
};
