import React from "react";
import "../loader/loader.css";

export const Loader = (): JSX.Element => {
  return (
    <div className="spinner">
      <div className="inner one"></div>
      <div className="inner two"></div>
      <div className="inner three"></div>
    </div>
  );
};
