export const urlPresence = "http://127.0.0.1:8000/api/presence";
export const urlChild = "http://127.0.0.1:8000/api/children";
export const urlFamily = "http://127.0.0.1:8000/api/family";
export const urlTransmission = "http://127.0.0.1:8000/api/transmission";
export const urlSleep = "http://127.0.0.1:8000/api/sleep";
export const urlMeal = "http://127.0.0.1:8000/api/meal";
export const urlPooh = "http://127.0.0.1:8000/api/pooh";
export const urlStaff = "http://127.0.0.1:8000/api/staff";
