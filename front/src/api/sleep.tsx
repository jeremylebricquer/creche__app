/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import axios from "axios";
import { Sleep } from "../Interfaces/Sleep";

export const getSleepById = (url: string) => {
  return new Promise<Sleep>((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        console.log("error", error);
        reject(error);
      });
  });
};
