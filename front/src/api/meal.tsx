/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import axios from "axios";
import { Meal } from "../Interfaces/Meal";

export const getMealById = (url: string) => {
  return new Promise<Meal>((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        console.log("error", error);
        reject(error);
      });
  });
};
