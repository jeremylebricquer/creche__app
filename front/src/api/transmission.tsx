/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import axios from "axios";
import { TransmissionData } from "../Interfaces/Transmission";

export const getTransmissionById = (url: string) => {
  return new Promise<{ data: TransmissionData[] }>((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        console.log("error", error);
        reject(error);
      });
  });
};
