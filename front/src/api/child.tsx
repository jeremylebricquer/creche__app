/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import axios from "axios";
import { Chld } from "../Interfaces/Children";
import { Presence } from "../Interfaces/Presence";

export const getPresence = (url: string) => {
  return new Promise<{ data: Presence[] }>((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        console.log("error", error);
        reject(error);
      });
  });
};

export const getChld = (url: string) => {
  return new Promise<Chld>((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        console.log("error", error);
        reject(error);
      });
  });
};
