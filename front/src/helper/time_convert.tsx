export const ConvertTime = (dateTime: string): string => {
  const dt = new Date(dateTime);
  const t = dt.toLocaleTimeString("fr-FR", {
    hour: "2-digit",
    minute: "2-digit",
  });
  return t;
};
