export interface TransmissionData {
  id: number;
  desc: string;
  children_id: number;
  staff_id: number;
  sleep_id: number;
  meal_id: number;
  pooh_id?: number;
}
