export interface Sleep {
  data: { id: number; desc: string; start: string; end: string };
}
