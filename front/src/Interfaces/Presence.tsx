export interface Presence {
  id: number;
  status: boolean;
  start: string;
  end: string;
  ids: number;
}
