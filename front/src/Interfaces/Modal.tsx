import { Chld } from "./Children";
import { Presence } from "./Presence";

export type ModalProps = {
  show: boolean;
  onHide: React.MouseEventHandler<HTMLButtonElement>;
  id: number;
};

export type PropsGlobal = {
  child: Chld;
  id: number;
  onHide: React.MouseEventHandler<HTMLButtonElement>;
  pres: Presence;
  show: boolean;
};
