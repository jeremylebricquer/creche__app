export interface Statut {
  id: number;
  value: string;
  label: string;
  prefix: string;
}
