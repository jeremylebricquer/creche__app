export interface Meal {
  data: {
    id: number;
    type: string;
    desc: string;
    timestamp: string;
  };
}
