export interface Chld {
  data: {
    id: number;
    name: string;
    firstname: string;
    birthday: Date;
    photos: string;
    gender: string;
    info_com: {
      childhood_diseases: string;
      alimetary_regime: string;
      sleep: string;
      weight: number;
      allergy: string;
      transmission: string;
    };
    family: string;
  };
}
