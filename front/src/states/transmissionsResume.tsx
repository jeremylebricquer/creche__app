import React, { useEffect, useState } from "react";
import { Loader } from "../component/loader/loader";
import { getTransmissionById } from "../api/transmission";
import { urlMeal, urlSleep, urlTransmission } from "../api/url";
import { TransmissionData } from "../Interfaces/Transmission";
import { Meal } from "../Interfaces/Meal";
import { getMealById } from "../api/meal";
import { Presence } from "../Interfaces/Presence";
import { Chld } from "../Interfaces/Children";
import { ConvertTime } from "../helper/time_convert";
import { getSleepById } from "../api/sleep";
import { Sleep } from "../Interfaces/Sleep";
import { GiBabyBottle, GiNightSleep } from "react-icons/gi";
import { BiTime, BiTimeFive } from "react-icons/bi";
import "../view/css/form.css";

type TransProps = {
  child: Chld[];
  pres: Presence[];
  id: number;
};

export const TransmissionsResume = (props: TransProps): JSX.Element => {
  const [trans, setTransdiv] = useState<TransmissionData[]>([]);
  const [meal, setMeal] = useState<Meal[]>([]);
  const [sleep, setSleep] = useState<Sleep[]>([]);
  const [load, setLoad] = useState(false);

  const presence = props.pres;

  //** API GET */
  useEffect(() => {
    const fetch = async () => {
      try {
        let meal_id = [];
        let sleep_id = [];
        const r_meal = [];
        const r_sleep: Sleep[] = [];
        const r_trans = await getTransmissionById(`${urlTransmission}`);
        const trans_filter = r_trans.data.filter(
          (f) => f.children_id === props.id
        );
        meal_id = trans_filter.map((c) => c.meal_id);
        sleep_id = trans_filter.map((c) => c.sleep_id);

        for (let i = 0; i < meal_id.length; i++) {
          r_meal.push(await getMealById(`${urlMeal}` + `/${meal_id[i]}`));
        }

        for (let i = 0; i < sleep_id.length; i++) {
          r_sleep.push(await getSleepById(`${urlSleep}` + `/${sleep_id[i]}`));
        }

        setTransdiv(trans_filter);
        setSleep([...sleep, ...r_sleep]);
        setMeal([...meal, ...r_meal]);

        setLoad(true);
      } catch (err) {
        console.log(err);
      }
    };
    fetch();
  }, []);

  const pres = presence.filter((t3) => t3.id === props.id);
  const concat = trans.map((t1) => ({
    ...t1,
    meal: meal.filter((t2: Meal) => t2.data.id === t1.meal_id),
    sleep: sleep.filter((t3: Sleep) => t3.data.id === t1.sleep_id),
  }));
  const start = ConvertTime(String(pres.map((c) => c.start)));
  const end = ConvertTime(String(pres.map((c) => c.end)));

  if (load) {
    return (
      <div className="container">
        <div className="trans_items">
          <BiTimeFive className="resume_trans_icon time" />
          <span>{start} : Arrivée</span>
        </div>

        {concat.map((c) =>
          c.sleep
            .sort((a, b) => a.data.start.localeCompare(b.data.start))
            .map((m) => (
              <div key={c.id} className="trans_items">
                <GiNightSleep className="resume_trans_icon sleep" />
                <span>
                  {ConvertTime(m.data.start)} - {ConvertTime(m.data.end)} :
                </span>
                <span> {m.data.desc} </span>
              </div>
            ))
        )}

        {concat.map((c) =>
          c.meal
            .sort((a, b) => a.data.timestamp.localeCompare(b.data.timestamp))
            .map((m) => (
              <div key={c.id} className="trans_items">
                <GiBabyBottle className="resume_trans_icon meal" />
                <span>
                  {m.data.timestamp.replace(/(:\d{2}| [AP]M)$/, "")} :{" "}
                </span>
                <span> {m.data.desc} </span>
              </div>
            ))
        )}

        <div className="trans_items">
          <BiTime className="resume_trans_icon time" />
          <span>{end} : Départ </span>
        </div>
      </div>
    );
  } else {
    return (
      <div className="container_spinner">
        <Loader />
      </div>
    );
  }
};
