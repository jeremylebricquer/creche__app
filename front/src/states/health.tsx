import React from "react";
import { Loader } from "../component/loader/loader";

export const Health = (): JSX.Element => {
  return (
    <div className="container_spinner">
      <h1>Santé</h1>
      <Loader />
    </div>
  );
};
