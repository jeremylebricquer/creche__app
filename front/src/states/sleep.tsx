import React from "react";
import { Loader } from "../component/loader/loader";

export const Sleep = (): JSX.Element => {
  return (
    <div className="container_spinner">
      <h1>Sieste</h1>
      <Loader />
    </div>
  );
};
