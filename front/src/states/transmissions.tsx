import React from "react";
import { Loader } from "../component/loader/loader";

export const Transmissions = (): JSX.Element => {
  return (
    <div className="container_spinner">
      <h1>Transmission</h1>
      <Loader />
    </div>
  );
};
