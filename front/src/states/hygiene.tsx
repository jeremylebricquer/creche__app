import React from "react";
import { Loader } from "../component/loader/loader";

export const Hygiene = (): JSX.Element => {
  return (
    <div className="container_spinner">
      <h1>Hygiene</h1>
      <Loader />
    </div>
  );
};
