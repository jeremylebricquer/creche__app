import React from "react";
import { Loader } from "../component/loader/loader";

export const Photos = (): JSX.Element => {
  return (
    <div className="container_spinner">
      <h1>Photos</h1>
      <Loader />
    </div>
  );
};
