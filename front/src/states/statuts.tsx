import React, { useEffect, useState } from "react";
import { getChld, getPresence } from "../api/child";
import { urlChild, urlPresence } from "../api/url";
import { Chld } from "../Interfaces/Children";
import { Presence } from "../Interfaces/Presence";
import { ViewTools } from "../view/viewTools";

export const Statuts: React.FC<Presence[] | Chld[]> = () => {
  const [present, setPresentList] = useState<Presence[]>([]);
  const [chld, setChldList] = useState<Chld[]>([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    const fetch = async () => {
      try {
        const r1 = await getPresence(urlPresence);
        setPresentList(r1.data);
        const ids = r1.data.map((c) => c.ids);
        const responses = [];
        for (let i = 0; i < ids.length; i++) {
          responses.push(await getChld(`${urlChild}` + `/${ids[i]}`));
        }
        setChldList(responses);
        setLoad(true);
      } catch (err) {
        console.log(err);
        setError(err);
      }
    };
    fetch();
  }, []);

  return (
    <ViewTools present={present} child={chld} isLoad={load} isError={error} />
  );
};
