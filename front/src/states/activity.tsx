import React from "react";
import { Loader } from "../component/loader/loader";

export const Activity = (): JSX.Element => {
  return (
    <div className="container_spinner">
      <h1>Activité</h1>
      <Loader />
    </div>
  );
};
