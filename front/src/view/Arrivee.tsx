import * as React from "react";
import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { VscCheck, VscChromeClose } from "react-icons/vsc";
import "../component/sleepReview/sleepReview.css";
import "../component/observation/observation.css";
import "../component/timePicker/timePicker.css";
import "../view/css/form.css";
import { Button } from "@material-ui/core";
import { PropsGlobal } from "../Interfaces/Modal";

export const Arrivee = (
  props: PropsGlobal,
  {
    onSubmit,
    store,
  }: {
    store: {
      arrivee: {
        time: string;
        sieste: string;
        observation: string;
      };
      depart: {
        time: string;
        observation: string;
      };
    };
    onSubmit: SubmitHandler<{
      arrivee: {
        observation: string;
        sieste: string;
        time: string;
      };
    }>;
  }
): JSX.Element => {
  const { handleSubmit, register } = useForm({
    defaultValues: store,
  });

  function getTime() {
    const time = new Date();
    const hour = time.getHours();
    const min = time.getMinutes();
    const completeTime = hour + ":" + min;
    return completeTime;
  }

  getTime();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [time, setTime] = useState<string>();
  const onChanged = (e: { target: { value: string } }) => {
    const value = e.target.value;
    setTime(value);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="formDetail">
        <div className="formElement">
          <div className="col-3">
            <label htmlFor="time">Heure d'arrivée : </label>
          </div>
          <div className="col-auto">
            <input
              type="time"
              id="time"
              {...register("arrivee.time")}
              onChange={onChanged}
            />
          </div>
        </div>

        <div className="formElement">
          <div className="col-3">
            <label htmlFor="sieste" className="form_title">
              Nuit / Sieste :
            </label>
          </div>

          <div className="col-auto">
            <ul className="flex-container longhand">
              <li className="flex-item">
                <input
                  className="medium"
                  type="radio"
                  value="Aucune"
                  {...register("arrivee.sieste")}
                />
                <label className="text">Aucune</label>
              </li>

              <li className="flex-item">
                <input
                  className="medium"
                  type="radio"
                  value="Courte"
                  {...register("arrivee.sieste")}
                />
                <label className="text">Courte</label>
              </li>

              <li className="flex-item">
                <input
                  className="medium"
                  type="radio"
                  value="Agitée"
                  {...register("arrivee.sieste")}
                />
                <label className="text">Agitée</label>
              </li>
              <li className="flex-item">
                <input
                  className="medium"
                  type="radio"
                  value="Bonne"
                  {...register("arrivee.sieste")}
                />
                <label className="text">Bonne</label>
              </li>
            </ul>
          </div>
        </div>

        <div className="formEl">
          <label htmlFor="observation" className="form_title">
            Observation :
          </label>
          <textarea {...register("arrivee.observation")} rows={5} cols={25} />
        </div>
        <div className="submit">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            style={{ margin: "0 10px 0 10px", fontSize: "larger" }}
          >
            <VscCheck />
            <span className="formBtn"> SAVE </span>
          </Button>
          <Button
            onClick={props.onHide}
            variant="contained"
            color="secondary"
            style={{ margin: "0 10px 0 10px", fontSize: "larger" }}
          >
            <VscChromeClose />
            <span className="formBtn "> CANCEL </span>
          </Button>
        </div>
      </div>
    </form>
  );
};
