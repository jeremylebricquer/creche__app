import { Button } from "@material-ui/core";
import * as React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { VscCheck, VscChromeClose } from "react-icons/vsc";
import { PropsGlobal } from "../Interfaces/Modal";
import { Transmission } from "./transmissionsView";
import "../component/observation/observation.css";
import "../component/timePicker/timePicker.css";
import "../view/css/form.css";

export const Depart = (
  props: PropsGlobal,
  {
    onSubmit,
    store,
  }: {
    store: {
      arrivee: {
        time: string;
        sieste: string;
        observation: string;
      };
      depart: {
        time: string;
        observation: string;
      };
    };
    onSubmit: SubmitHandler<{
      depart: {
        observation: string;
        time: string;
      };
    }>;
  }
): JSX.Element => {
  const { handleSubmit, register } = useForm({
    defaultValues: store,
  });

  function getTime() {
    const time = new Date();
    const hour = time.getHours();
    const min = time.getMinutes();
    const completeTime = hour + ":" + min;
    return completeTime;
  }

  getTime();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [time, setTime] = React.useState<string>();
  const onChanged = (e: { target: { value: string } }) => {
    const value = e.target.value;
    setTime(value);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="formDetail">
        <div className="formElement">
          <div className="col-3">
            <label htmlFor="time" className="form_title">
              Heure de départ
            </label>
          </div>
          <div className="col-auto">
            <input
              type="time"
              id="time"
              value={time}
              {...register("depart.time")}
              onChange={onChanged}
            />
          </div>
        </div>

        <div className="formEl">
          <label htmlFor="observation" className="form_title">
            Observation :
          </label>
          <textarea {...register("depart.observation")} rows={2} cols={25} />
        </div>

        <div className="formEl">
          <div className="fieldset">
            <h1>Transmission</h1>
            <Transmission {...props} />
          </div>
        </div>
        <div className="submit">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            style={{ margin: "0 10px 0 10px", fontSize: "larger" }}
          >
            <VscCheck /> <span className="formBtn "> SAVE </span>
          </Button>
          <Button
            onClick={props.onHide}
            variant="contained"
            color="secondary"
            style={{ margin: "0 10px 0 10px", fontSize: "larger" }}
          >
            <VscChromeClose />
            <span className="formBtn "> CANCEL </span>
          </Button>
        </div>
      </div>
    </form>
  );
};
