import React from "react";
import { TransmissionsResume } from "../states/transmissionsResume";

export const Transmission = (props: any): JSX.Element => {
  return <TransmissionsResume {...props} />;
};
