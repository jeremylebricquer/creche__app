import React from "react";
import { ConvertTime } from "../helper/time_convert";
import { ViewListProps } from "./viewList";

export const ViewPointing = (start: ViewListProps): JSX.Element => {
  return (
    <div className="col-auto">
      <div className="row align-items-center">
        <span>Arrivée : {ConvertTime(start.start)}</span>
        <span>Départ: {ConvertTime(start.end)}</span>
      </div>
    </div>
  );
};
