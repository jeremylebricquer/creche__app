import React, { Fragment, useState } from "react";
import { ModalEdit } from "../component/modal/modalEdit";
import { Chld } from "../Interfaces/Children";
import { Presence } from "../Interfaces/Presence";
import { ViewPointing } from "./viewPointing";
import { RiEdit2Line } from "react-icons/ri";

export type ViewListProps = {
  start: string;
  end: string;
};

export const ViewList = (props: {
  present: Presence[];
  child: Chld[];
  select: number;
}): JSX.Element => {
  const childTab = props.present.map((t1) => ({
    ...t1,
    ...props.child.find((t2: Chld) => t2.data.id === t1.ids),
  }));

  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const [id, setId] = useState(0);

  if (props.select === 1) {
    return (
      <div className="row">
        {childTab.map((c) => (
          <Fragment key={c.ids}>
            {c.status ? (
              <div className="card mb-2">
                <ul className="list-group list-group-flush list m-3">
                  <li className="list-group-item px-0">
                    <div className="row align-items-center">
                      <div className="col-1">
                        <div className="row align-items-center">
                          <span className="dot isPresent"></span>
                        </div>
                      </div>
                      <div className="col-auto">
                        <a href="#" className="avatar rounded-circle">
                          <img src={c.data?.photos} alt="new"></img>
                        </a>
                      </div>

                      <div className="col-4">
                        <div className="row align-items-center">
                          <h4 className="mb-0">{c.data?.name}</h4>
                          <h3 className="mb-0">{c.data?.firstname}</h3>
                        </div>
                      </div>
                      <ViewPointing start={c.start} end={c.end} />
                      <div className="col-auto">
                        <RiEdit2Line
                          onClick={() => {
                            handleShow();
                            setId(c.ids);
                          }}
                          className="btn-icon"
                        ></RiEdit2Line>
                      </div>
                      <ModalEdit
                        show={show}
                        onHide={() => setShow(false)}
                        id={id}
                        pres={props.present}
                        child={props.child}
                      />
                    </div>
                  </li>
                </ul>
              </div>
            ) : null}
          </Fragment>
        ))}
      </div>
    );
  } else if (props.select === 0) {
    return (
      <div className="row">
        {childTab.map((c) => (
          <Fragment key={c.ids}>
            {Number(c.status) === 0 ? (
              <div className="card mb-2">
                <ul className="list-group list-group-flush list m-3">
                  <li className="list-group-item px-0">
                    <div className="row align-items-center">
                      <div className="col-1 ">
                        <div className="row align-items-center">
                          <span className="dot isAbsent"></span>
                        </div>
                      </div>

                      <div className="col-auto">
                        <a href="#" className="avatar rounded-circle">
                          <img src={c.data?.photos} alt="new"></img>
                        </a>
                      </div>

                      <div className="col-4">
                        <div className="row align-items-center">
                          <h4 className="mb-0">{c.data?.name}</h4>
                          <h3 className="mb-0">{c.data?.firstname}</h3>
                        </div>
                      </div>
                      <ViewPointing start={c.start} end={c.end} />
                    </div>
                  </li>
                </ul>
              </div>
            ) : null}
          </Fragment>
        ))}
      </div>
    );
  } else {
    return <div> Pas d'enfants aujourd'hui</div>;
  }
};
