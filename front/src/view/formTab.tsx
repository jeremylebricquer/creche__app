import React from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Arrivee } from "./arrivee";
import { Depart } from "./depart";
import Box from "@material-ui/core/Box";
import { VscChromeClose } from "react-icons/vsc";
import { Button } from "@material-ui/core";
import "../view/css/form.css";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

export const SimpleTabs = (props: any): JSX.Element => {
  const [store, setStore] = React.useState({
    arrivee: {
      time: "",
      sieste: "",
      observation: "",
    },
    depart: {
      time: "",
      observation: "",
    },
  });
  const [value, setValue] = React.useState(0);

  // eslint-disable-next-line @typescript-eslint/ban-types
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const onSubmit = (
    data: Partial<{
      arrivee: {
        time: string;
        sieste: string;
        observation: string;
      };
      depart: {
        time: string;
        observation: string;
      };
    }>
  ) => {
    setStore({
      ...store,
      ...data,
    });
  };

  return (
    <div>
      <div>
        <Tabs value={value} onChange={handleChange} aria-label="tabs" centered>
          <Tab label="Arrivee" />
          <Tab label="Depart" />
        </Tabs>
      </div>
      <TabPanel value={value} index={0}>
        <Arrivee onSubmit={onSubmit} store={store} {...props} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Depart onSubmit={onSubmit} store={store} {...props} />
      </TabPanel>
    </div>
  );
};
