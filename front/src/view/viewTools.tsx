import React, { useState } from "react";
import { Loader } from "../component/loader/loader";
import { Dropdown } from "react-bootstrap";
import { ViewList } from "./viewList";
import { Chld } from "../Interfaces/Children";
import { Presence } from "../Interfaces/Presence";
import { Statut } from "../Interfaces/Status";
import "./css/viewList.css";

const values: Statut[] = [
  { id: 0, value: "false", label: "absents", prefix: "Vous affichez les " },
  { id: 1, value: "true", label: "présents", prefix: "Vous affichez les " },
];

type CustomToggleProps = {
  children: React.ReactNode;
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
};

const CustomToggle = React.forwardRef(
  (props: CustomToggleProps, ref: React.Ref<HTMLAnchorElement>) => (
    <a
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        props.onClick(e);
      }}
    >
      {props.children}
      <span style={{ paddingLeft: "5px" }}>&#x25bc;</span>
    </a>
  )
);

type CustomMenuProps = {
  children: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
  labeledBy?: string;
};

const CustomMenu = React.forwardRef(
  (props: CustomMenuProps, ref: React.Ref<HTMLDivElement>) => {
    const [value] = useState("");

    return (
      <div
        ref={ref}
        style={props.style}
        className={props.className}
        aria-labelledby={props.labeledBy}
      >
        <ul className="list-unstyled">
          {React.Children.toArray(props.children).filter(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (child: any) =>
              !value || child.props.children.toLowerCase().startsWith(value)
          )}
        </ul>
      </div>
    );
  }
);

export const ViewTools = (props: {
  present: Presence[];
  child: Chld[];
  isLoad: boolean;
  isError: string;
}): JSX.Element => {
  const [selectedStatut, setSeletedStatut] = useState<number>(1);

  const handleSelect = (e: string | null) => {
    setSeletedStatut(Number(e));
  };

  const TheChosenStatut = () => {
    const chosenStatut = values.find((f) => f.id === selectedStatut);
    return chosenStatut
      ? chosenStatut.prefix + chosenStatut.label
      : "Selectionnez un status";
  };

  if (props.isLoad) {
    return (
      <div className="container-fluid mt-3">
        <div className="col auto">
          <h1 className="Menutitle">Pointage</h1>
          <div className="row mb-5 ">
            <div
              className="toolbar"
              style={{ display: "flex", flexDirection: "row-reverse" }}
            >
              <Dropdown onSelect={handleSelect}>
                <Dropdown.Toggle
                  as={CustomToggle}
                  id="dropdown-custom-components"
                >
                  {TheChosenStatut()}
                </Dropdown.Toggle>
                <Dropdown.Menu as={CustomMenu}>
                  {values.map((s) => {
                    return (
                      <Dropdown.Item key={s.id} eventKey={s.id.toString()}>
                        {s.label}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
              <span style={{ paddingRight: "2%" }}>
                {" "}
                Nb enfant total:{props.present.length}
              </span>
            </div>
          </div>
          <ViewList {...props} select={selectedStatut} />
        </div>
      </div>
    );
  } else {
    return (
      <div className="container_spinner">
        <Loader />
      </div>
    );
  }
};
