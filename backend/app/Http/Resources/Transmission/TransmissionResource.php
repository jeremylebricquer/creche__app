<?php

namespace App\Http\Resources\Transmission;

use Illuminate\Http\Resources\Json\JsonResource;

class TransmissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'desc' => $this->desc,
            'children_id' => $this->children_id,
            'staff_id' => $this->staff_id,
            'meal_id' => $this->meal_id,
            'sleep_id' => $this->sleep_id,
            'pooh_id' => $this->pooh_id,
        ];
    }
}
