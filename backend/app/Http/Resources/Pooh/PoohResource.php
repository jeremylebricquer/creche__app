<?php

namespace App\Http\Resources\Pooh;

use Illuminate\Http\Resources\Json\JsonResource;

class PoohResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'desc' => $this->desc,
            'timestamp' => $this->timestamp,
        ];
    }
}
