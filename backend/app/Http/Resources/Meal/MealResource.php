<?php

namespace App\Http\Resources\Meal;

use Illuminate\Http\Resources\Json\JsonResource;

class MealResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'desc' => $this->desc,
            'timestamp' => $this->timestamp,
        ];
    }
}
