<?php

namespace App\Http\Resources\Executive;

use Illuminate\Http\Resources\Json\JsonResource;

class ExecutiveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'firstname' => $this->firstname,
            'creche' => $this->creche_id,
        ];
    }
}
