<?php

namespace App\Http\Resources\Sleep;

use Illuminate\Http\Resources\Json\JsonResource;

class SleepResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start' => $this->start,
            'end' => $this->end,
            'desc' => $this->desc,

        ];
    }
}
