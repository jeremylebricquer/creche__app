<?php

namespace App\Http\Resources\Creche;

use Illuminate\Http\Resources\Json\JsonResource;

class CrecheResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'contract' => $this->contract_id,
        ];
    }
}
