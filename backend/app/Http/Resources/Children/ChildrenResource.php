<?php

namespace App\Http\Resources\Children;

use Illuminate\Http\Resources\Json\JsonResource;

class ChildrenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)

    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'firstname' => $this->firstname,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'photos' => $this->photos,
            'info_comp' => [
                'childhood_diseases' => $this->childhood_diseases,
                'alimetary_regime' => $this->alimetary_regime,
                'weight' => $this->weight,
                'allergy' => $this->allergy,
            ],
            'family' => $this->family_id,
        ];
    }
}
