<?php

namespace App\Http\Resources\Presence;

use Illuminate\Http\Resources\Json\JsonResource;

class PresenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'start' => $this->start,
            'end' => $this->end,
            'ids' => $this->children_id,

        ];
    }
}
