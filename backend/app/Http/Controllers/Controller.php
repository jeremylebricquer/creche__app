<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *    title="API documentation by resource",
     *    version="1.0.0",
     *    description="Api documentation of one creche",
     * ),
     *     @OA\Tag(
     *     name="Children",
     *     description="API Endpoints of Child"
     * ),
     *     @OA\Tag(
     *     name="Contact",
     *     description="API Endpoints of Contact"
     * ),
     *     @OA\Tag(
     *     name="Contract",
     *     description="API Endpoints of Contract"
     * ),
     *     @OA\Tag(
     *     name="Creche",
     *     description="API Endpoints of Creche"
     * ),
     *     @OA\Tag(
     *     name="Executive",
     *     description="API Endpoints of Executive"
     * ),
     *     @OA\Tag(
     *     name="Family",
     *     description="API Endpoints of Family"
     * ),
     *     @OA\Tag(
     *     name="Invoices",
     *     description="API Endpoints of Invoices"
     * ),
     *     @OA\Tag(
     *     name="Roles",
     *     description="API Endpoints of Roles"
     * ),
     *    @OA\Tag(
     *     name="Staff",
     *     description="API Endpoints of Staff"
     * ),
     *     @OA\Tag(
     *     name="User",
     *     description="API Endpoints of User"
     * ),
     */
}
