<?php

namespace App\Http\Controllers\Api;

use App\Models\Pooh;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Pooh\PoohResource;
use App\Http\Resources\Pooh\PoohCollection;

class PoohController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/pooh",
     *     tags={"Pooh"},
     *     summary="Get list of pooh",
     *     description="Display all pooh",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new PoohCollection(Pooh::all());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/pooh",
     *      tags={"Pooh"},
     *      summary="Add pooh",
     *      @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="timestamp",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Pooh::create($request->all())) {
            return  response()->json([
                'success' => 'Hygiene ajouté avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pooh $pooh
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/pooh/{id}",
     *     tags={"Pooh"},
     *     summary="Selected pooh",
     *     description="Display a pooh selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show($id)
    {
        $pooh = Pooh::Find($id);

        if ($pooh !== null) {
            return new PoohResource($pooh);
        } else {
            return response()->json(['data' => 'Resource not found'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pooh  $pooh
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/pooh/{id}",
     *      tags={"Pooh"},
     *      summary="Update pooh",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="timestamp",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, $id)
    {
        $pooh = Pooh::findOrFail($id);
        if ($pooh->update($request->all())) {
            return response()->json([
                'success' => 'Hygiene modifié avec succès'
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pooh $pooh
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/pooh/{id}",
     *      tags={"Pooh"},
     *      summary="Delete pooh",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy($id)
    {
        $pooh = Pooh::findOrFail($id);
        $pooh->delete();
    }
}
