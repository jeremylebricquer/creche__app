<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Contact\ContactResource;
use App\Http\Resources\Contact\ContactCollection;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/contact",
     *     tags={"Contact"},
     *     summary="Get list of contact",
     *     description="Display all contact",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new ContactCollection(Contact::all());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/contact",
     *      tags={"Contact"},
     *      summary="Add Contact",
     *      @OA\Parameter(
     *          name="phone",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="adress",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="family_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="staff_id",
     *          in="query",
     *          required=true, 
     *      ),
     *     @OA\Parameter(
     *          name="executive_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Contact::create($request->all())) {
            return  response()->json([
                'success' => "Contact ajouté avec succès"
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/contact/{id}",
     *     tags={"Contact"},
     *     summary="Selected contact",
     *     description="Display a contact selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(Contact $contact)
    {
        return new ContactResource($contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/contact/{id}",
     *      tags={"Contact"},
     *      summary="Update Contact",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="phone",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="adress",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="family_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="staff_id",
     *          in="query",
     *          required=true, 
     *      ),
     *     @OA\Parameter(
     *          name="executive_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, Contact $contact)
    {
        if ($contact->update($request->all())) {
            return response()->json([
                'success' => "Contact modifié avec succès"
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/contact/{id}",
     *      tags={"Contact"},
     *      summary="Delete Contact",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Request $request, Contact $contact)
    {
        if ($contact->delete($request->all())) {
            return response()->json([
                'success' => "Contact supprimé avec succès"
            ], 200);
        }
    }
}
