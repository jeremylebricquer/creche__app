<?php

namespace App\Http\Controllers\Api;

use App\Models\Meal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Meal\MealResource;
use App\Http\Resources\Meal\MealCollection;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/meal",
     *     tags={"Meal"},
     *     summary="Get list of meal",
     *     description="Display all meal",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new MealCollection(Meal::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/meal",
     *      tags={"Meal"},
     *      summary="Add meal",
     *      @OA\Parameter(
     *          name="type",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="timestamp",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Meal::create($request->all())) {
            return  response()->json([
                'success' => 'Repas ajouté avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meal  $meal
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/meal/{id}",
     *     tags={"Meal"},
     *     summary="Selected meal",
     *     description="Display a meal selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show($id)
    {
        $meal = Meal::Find($id);

        if ($meal !== null) {
            return new MealResource($meal);
        } else {
            return response()->json(['data' => 'Resource not found'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Meal  $meal
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/meal/{id}",
     *      tags={"Meal"},
     *      summary="Update meal",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="type",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="timestamp",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, $id)
    {
        $meal = Meal::findOrFail($id);
        if ($meal->update($request->all())) {
            return response()->json([
                'success' => 'Repas modifié avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meal  $meal
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/meal/{id}",
     *      tags={"Meal"},
     *      summary="Delete meal",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy($id)
    {
        $meal = Meal::findOrFail($id);
        $meal->delete();
    }
}
