<?php

namespace App\Http\Controllers\API;

use App\Models\Presence;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Presence\PresenceResource;
use App\Http\Resources\Presence\PresenceCollection;

class PresenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/presence",
     *     tags={"Presence"},
     *     summary="Get list of children present",
     *     description="Display all child present",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */


    public function index()
    {
        return new PresenceCollection(Presence::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/presence",
     *      tags={"Presence"},
     *      summary="Add present status for child",
     *      @OA\Parameter(
     *          name="status",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="start",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="end",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */

    public function store(Request $request)
    {
        if (Presence::create($request->all())) {
            return  response()->json([
                'success' => 'Pointage ajoutée avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/presence/{id}",
     *     tags={"Presence"},
     *     summary="Selected child present",
     *     description="Display a child present select by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */

    public function show(Presence $presence)
    {
        return new PresenceResource($presence);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Put(
     *      path="/api/presence/{id}",
     *      tags={"Presence"},
     *      summary="Update Child present",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="status",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="start",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="end",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */

    public function update(Request $request, Presence $presence)
    {
        if ($presence->update($request->all())) {
            return response()->json([
                'success' => 'Pointage modifiée avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Delete(
     *      path="/api/presence/{id}",
     *      tags={"Presence"},
     *      summary="Delete Child present",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Request $request, Presence $presence)
    {
        if ($presence->delete($request->all())) {
            return response()->json([
                'success' => 'Pointage supprimée avec succès'
            ], 200);
        }
    }
}
