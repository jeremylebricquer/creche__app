<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\UserCollection;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/user",
     *     tags={"User"},
     *     summary="Get list of user",
     *     description="Display all user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new UserCollection(User::all());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/user",
     *      tags={"User"},
     *      summary="Add User",
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="role_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (User::create($request->all())) {
            return  response()->json([
                'success' => "User ajoutée avec succès"
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/user/{id}",
     *     tags={"User"},
     *     summary="Selected user",
     *     description="Display a user selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/user/{id}",
     *      tags={"User"},
     *      summary="Update User",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="password",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="role_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, User $user)
    {
        if ($user->update($request->all())) {
            return response()->json([
                'success' => "User modifié avec succès"
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/user/{id}",
     *      tags={"User"},
     *      summary="Delete User",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Request $request,  User $user)
    {
        if ($user->delete($request->all())) {
            return response()->json([
                'success' => "User supprimé avec succès"
            ], 200);
        }
    }
}
