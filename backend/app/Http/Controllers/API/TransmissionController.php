<?php

namespace App\Http\Controllers\Api;

use App\Models\Pooh;
use App\Models\Transmission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Transmission\TransmissionResource;
use App\Http\Resources\Transmission\TransmissionCollection;

class TransmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/transmission",
     *     tags={"Transmission"},
     *     summary="Get list of transmission",
     *     description="Display all transmission",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new TransmissionCollection(Transmission::all());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/transmission",
     *      tags={"Transmission"},
     *      summary="Add trans",
     *      @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="children_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="staff_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="meal_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="sleep_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="pooh_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Transmission::create($request->all())) {
            return  response()->json([
                'success' => 'Transmission ajouté avec succès'
            ], 200);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transmission $trans
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/transmission/{id}",
     *     tags={"Transmission"},
     *     summary="Selected trans",
     *     description="Display a trans selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show($id)
    {
        $trans = Transmission::Find($id);

        if ($trans !== null) {
            return new TransmissionResource($trans);
        } else {
            return response()->json(['data' => 'Resource not found'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transmission  $trans
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/transmission/{id}",
     *      tags={"Transmission"},
     *      summary="Update transmission",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="children_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="staff_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="meal_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="sleep_id",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="pooh_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, $id)
    {
        $trans = Transmission::findOrFail($id);
        if ($trans->update($request->all())) {
            return response()->json([
                'success' => 'Transmission modifié avec succès'
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transmission $trans
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/transmission/{id}",
     *      tags={"Transmission"},
     *      summary="Delete transmission",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy($id)
    {
        $trans = Transmission::findOrFail($id);
        $trans->delete();
    }
}
