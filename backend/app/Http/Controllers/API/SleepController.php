<?php

namespace App\Http\Controllers\Api;

use App\Models\Sleep;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Sleep\SleepResource;
use App\Http\Resources\Sleep\SleepCollection;

class SleepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/sleep",
     *     tags={"Sleep"},
     *     summary="Get list of sleep",
     *     description="Display all sleep",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new SleepCollection(Sleep::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/sleep",
     *      tags={"Sleep"},
     *      summary="Add sleep",
     *      @OA\Parameter(
     *          name="start",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *      @OA\Parameter(
     *          name="end",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *       @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Sleep::create($request->all())) {
            return  response()->json([
                'success' => 'Sieste ajouté avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sleep  $sleep
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/sleep/{id}",
     *     tags={"Sleep"},
     *     summary="Selected sleep",
     *     description="Display a sleep selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show($id)
    {
        $sleep = Sleep::Find($id);

        if ($sleep !== null) {
            return new SleepResource($sleep);
        } else {
            return response()->json(['data' => 'Resource not found'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Sleep  $sleep
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/sleep/{id}",
     *      tags={"Sleep"},
     *      summary="Update sleep",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="start",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *      @OA\Parameter(
     *          name="end",
     *          in="query",
     *          required=true, 
     *          example="2021-05-08 11:31:50",
     *      ),
     *       @OA\Parameter(
     *          name="desc",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, $id)
    {
        $sleep = Sleep::findOrFail($id);
        if ($sleep->update($request->all())) {
            return response()->json([
                'success' => 'Sieste modifié avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sleep $sleep
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/sleep/{id}",
     *      tags={"Sleep"},
     *      summary="Delete sleep",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy($id)
    {
        $sleep = Sleep::findOrFail($id);
        $sleep->delete();
    }
}
