<?php

namespace App\Http\Controllers\Api;

use App\Models\Contract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Contract\ContractResource;
use App\Http\Resources\Contract\ContractCollection;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/contract",
     *     tags={"Contract"},
     *     summary="Get list of contract",
     *     description="Display all contract",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new ContractCollection(Contract::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/contract",
     *      tags={"Contract"},
     *      summary="Add Contract",
     *      @OA\Parameter(
     *          name="contractNb",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="contractChildCount",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Contract::create($request->all())) {
            return  response()->json([
                'success' => 'Contrat ajouté avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/contract/{id}",
     *     tags={"Contract"},
     *     summary="Selected contract",
     *     description="Display a contract selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(Contract $contract)
    {
        return new ContractResource($contract);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/contract/{id}",
     *      tags={"Contract"},
     *      summary="Update Contract",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="contractNb",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="contractChildCount",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, Contract $contract)
    {
        if ($contract->update($request->all())) {
            return response()->json([
                'success' => 'Contrat modifié avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/contract/{id}",
     *      tags={"Contract"},
     *      summary="Delete Contract",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Request $request, Contract $contract)
    {
        if ($contract->delete($request->all())) {
            return response()->json([
                'success' => 'Contrat supprimé avec succès'
            ], 200);
        }
    }
}
