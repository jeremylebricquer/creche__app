<?php

namespace App\Http\Controllers\Api;

use App\Models\Children;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Children\ChildrenCollection;
use App\Http\Resources\Children\ChildrenResource;

class ChildrenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/children",
     *     tags={"Children"},
     *     summary="Get list of children",
     *     description="Display all children",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new ChildrenCollection(Children::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/children",
     *      tags={"Children"},
     *      summary="Add Child",
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="birthday",
     *          in="query",
     *          required=true, 
     *          example="2020-04-03",
     *      ),
     *       @OA\Parameter(
     *          name="family_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Children::create($request->all())) {
            return  response()->json([
                'success' => 'Enfant ajouté avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Children  $children
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/children/{id}",
     *     tags={"Children"},
     *     summary="Selected child",
     *     description="Display a child selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(Children $children, $id)
    {
        $children = Children::Find($id);

        if ($children !== null) {
            return new ChildrenResource($children);
        } else {
            return response()->json(['data' => 'Resource not found'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Children  $children
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/children/{id}",
     *      tags={"Children"},
     *      summary="Update Child",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="birthday",
     *          in="query",
     *          required=true, 
     *          example="2020-04-03",
     *      ),
     *       @OA\Parameter(
     *          name="family_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, Children $children, $id)
    {
        $children = Children::findOrFail($id);
        if ($children->update($request->all())) {
            return response()->json([
                'success' => 'Enfant modifié avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Children  $children
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/children/{id}",
     *      tags={"Children"},
     *      summary="Delete Child",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Children $children, $id)
    {
        $children = Children::findOrFail($id);
        $children->delete();
    }
}
