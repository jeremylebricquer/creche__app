<?php

namespace App\Http\Controllers\Api;

use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Invoice\InvoiceResource;
use App\Http\Resources\Invoice\InvoiceCollection;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/invoice",
     *     tags={"Invoices"},
     *     summary="Get list of invoices",
     *     description="Display all invoices",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new InvoiceCollection(Invoice::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/invoice",
     *      tags={"Invoices"},
     *      summary="Add Child",
     *      @OA\Parameter(
     *          name="amount",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="date",
     *          in="query",
     *          required=true, 
     *          example="2018-11-16",
     *      ),
     *       @OA\Parameter(
     *          name="status",
     *          in="query",
     *          required=true, 
     *          example="boolean",
     *      ),
     *       @OA\Parameter(
     *          name="contract_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Invoice::create($request->all())) {
            return  response()->json([
                'success' => 'Facture ajoutée avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/invoice/{id}",
     *     tags={"Invoices"},
     *     summary="Selected child",
     *     description="Display a child selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(Invoice $invoice)
    {
        return new InvoiceResource($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/invoice/{id}",
     *      tags={"Invoices"},
     *      summary="Update Child",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="amount",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="date",
     *          in="query",
     *          required=true, 
     *          example="2018-11-16",
     *      ),
     *       @OA\Parameter(
     *          name="status",
     *          in="query",
     *          required=true, 
     *          example="boolean",
     *      ),
     *       @OA\Parameter(
     *          name="contract_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, Invoice $invoice)
    {
        if ($invoice->update($request->all())) {
            return response()->json([
                'success' => 'Facture modifiée avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/invoice/{id}",
     *      tags={"Invoices"},
     *      summary="Delete Child",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Request $request, Invoice $invoice)
    {
        if ($invoice->delete($request->all())) {
            return response()->json([
                'success' => 'Facture supprimée avec succès'
            ], 200);
        }
    }
}
