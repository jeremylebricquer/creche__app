<?php

namespace App\Http\Controllers\Api;

use App\Models\Family;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Family\FamilyResource;
use App\Http\Resources\Family\FamilyCollection;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/family",
     *     tags={"Family"},
     *     summary="Get list of family",
     *     description="Display all family",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new FamilyCollection(Family::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/family",
     *      tags={"Family"},
     *      summary="Add Family",
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="contract_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */

    public function store(Request $request)
    {
        if (Family::create($request->all())) {
            return  response()->json([
                'success' => 'Famille ajoutée avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Family  $family
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *     path="/api/family/{id}",
     *     tags={"Family"},
     *     summary="Selected family",
     *     description="Display a family select by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(Family $family)
    {
        return new FamilyResource($family);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Family  $family
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Put(
     *      path="/api/family/{id}",
     *      tags={"Family"},
     *      summary="Update Family",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="contract_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */

    public function update(Request $request, Family $family)
    {
        if ($family->update($request->all())) {
            return response()->json([
                'success' => 'Famille modifiée avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Family  $family
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/family/{id}",
     *      tags={"Family"},
     *      summary="Delete Family",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */

    public function destroy(Request $request, Family $family)
    {
        if ($family->delete($request->all())) {
            return response()->json([
                'success' => 'Famille supprimée avec succès'
            ], 200);
        }
    }
}
