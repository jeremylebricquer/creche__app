<?php

namespace App\Http\Controllers\Api;

use App\Models\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Staff\StaffResource;
use App\Http\Resources\Staff\StaffCollection;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/staff",
     *     tags={"Staff"},
     *     summary="Get list of staff",
     *     description="Display all staff",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new StaffCollection(Staff::all());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/staff",
     *      tags={"Staff"},
     *      summary="Add Staff",
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="planning",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="fonction",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="executive_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Staff::create($request->all())) {
            return  response()->json([
                'success' => "Membre de l'équipe ajoutée avec succès"
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/staff/{id}",
     *     tags={"Staff"},
     *     summary="Selected staff",
     *     description="Display a staff selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(Staff $staff)
    {
        return new StaffResource($staff);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/staff/{id}",
     *      tags={"Staff"},
     *      summary="Update Staff",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="planning",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="fonction",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="executive_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, Staff $staff)
    {
        if ($staff->update($request->all())) {
            return response()->json([
                'success' => "Membre de l'équipe modifié avec succès"
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/staff/{id}",
     *      tags={"Staff"},
     *      summary="Delete Staff",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Request $request, Staff $staff)
    {
        if ($staff->delete($request->all())) {
            return response()->json([
                'success' => "Membre de l'équipe supprimé avec succès"
            ], 200);
        }
    }
}
