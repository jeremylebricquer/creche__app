<?php

namespace App\Http\Controllers\Api;

use App\Models\Executive;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Executive\ExecutiveResource;
use App\Http\Resources\Executive\ExecutiveCollection;

class ExecutiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/executive",
     *     tags={"Executive"},
     *     summary="Get list of executive",
     *     description="Display all executive",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function index()
    {
        return new ExecutiveCollection(Executive::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *      path="/api/executive",
     *      tags={"Executive"},
     *      summary="Add Executive",
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="creche_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function store(Request $request)
    {
        if (Executive::create($request->all())) {
            return  response()->json([
                'success' => 'Direction ajoutée avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Executive  $executive
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/api/executive/{id}",
     *     tags={"Executive"},
     *     summary="Selected Executive",
     *     description="Display a Executive selected by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function show(Executive $executive)
    {
        return new ExecutiveResource($executive);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Executive  $executive
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/executive/{id}",
     *      tags={"Executive"},
     *      summary="Update Executive",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="firstname",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="creche_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, Executive $executive)
    {
        if ($executive->update($request->all())) {
            return response()->json([
                'success' => 'Direction modifiée avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Executive  $executive
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Delete(
     *      path="/api/executive/{id}",
     *      tags={"Executive"},
     *      summary="Delete Executive",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */
    public function destroy(Request $request, Executive $executive)
    {
        if ($executive->delete($request->all())) {
            return response()->json([
                'success' => 'Direction supprimée avec succès'
            ], 200);
        }
    }
}
