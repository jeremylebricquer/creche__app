<?php

namespace App\Http\Controllers\Api;

use App\Models\Creche;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Creche\CrecheResource;
use App\Http\Resources\Creche\CrecheCollection;

class CrecheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *     path="/api/creche",
     *     tags={"Creche"},
     *     summary="Get list of creche",
     *     description="Display all creche",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */

    public function index()
    {
        return new CrecheCollection(Creche::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/creche",
     *      tags={"Creche"},
     *      summary="Add Creche",
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="contract_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */




    public function store(Request $request)
    {
        if (Creche::create($request->all())) {
            return  response()->json([
                'success' => 'Creche ajoutée avec succès'
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Creche  $creche
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *     path="/api/creche/{id}",
     *     tags={"Creche"},
     *     summary="Selected creche",
     *     description="Display a creche select by id",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */

    public function show(Creche $creche)
    {
        return new CrecheResource($creche);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Creche  $creche
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Put(
     *      path="/api/creche/{id}",
     *      tags={"Creche"},
     *      summary="Update Creche",
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          in="query",
     *          required=true, 
     *      ),
     *       @OA\Parameter(
     *          name="contract_id",
     *          in="query",
     *          required=true, 
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     * )
     */
    public function update(Request $request, Creche $creche)
    {
        if ($creche->update($request->all())) {
            return response()->json([
                'success' => 'Creche modifiée avec succès'
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Creche  $creche
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Delete(
     *      path="/api/creche/{id}",
     *      tags={"Creche"},
     *      summary="Delete Creche",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *      ),
     *        @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     * )
     */



    public function destroy(Request $request, Creche $creche)
    {
        if ($creche->delete($request->all())) {
            return response()->json([
                'success' => 'Creche supprimée avec succès'
            ], 200);
        }
    }
}
