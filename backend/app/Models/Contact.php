<?php

namespace App\Models;

use App\Models\Staff;
use App\Models\Family;
use App\Models\Executive;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = ['phone', 'adress', 'email', 'family_id'];

    public function family()
    {
        return $this->hasMany(Family::class);
    }
}
