<?php

namespace App\Models;

use App\Models\Contract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Creche extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'contract_id'];

    public function contracts()
    {
        return $this->hasMany(Contract::class, 'contract_id');
    }

    public function invoices()
    {
        return $this->hasMany(Contract::class);
    }

    public function executives()
    {
        return $this->hasMany(executive::class, 'creche_id');
    }
}
