<?php

namespace App\Models;

use App\Models\Transmission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pooh extends Model
{
    use HasFactory;

    protected $fillable = ['desc', 'timestamp'];

    public function tranmissions()
    {
        return $this->hasMany(Transmission::class);
    }
}
