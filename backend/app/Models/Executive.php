<?php

namespace App\Models;

use App\Models\Staff;
use App\Models\Creche;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Executive extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'firstname', 'creche_id'];

    public function creches()
    {
        return $this->hasMany(Creche::class);
    }

    public function staff()
    {
        return $this->hasOne(Staff::class);
    }
}
