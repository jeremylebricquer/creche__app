<?php

namespace App\Models;

use App\Models\Staff;
use App\Models\Family;
use App\Models\Presence;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Children extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'firstname', 'photos', 'gender', 'birthday', 'childhood_diseases', 'alimetary_regime', 'weight', 'allergy', 'family_id'];

    public function family()
    {
        return $this->hasOne(Family::class);
    }
    public function staff()
    {
        return $this->hasOne(Staff::class);
    }
    public function presence()
    {
        return $this->hasOne(Presence::class);
    }
}
