<?php

namespace App\Models;

use App\Models\User;
use App\Models\Children;
use App\Models\Contract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Family extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'firstname', 'contract_id'];

    public function contracts()
    {
        return $this->hasOne(Contract::class, 'contract_id');
    }

    public function childrens()
    {
        return $this->hasMany(Children::class);
    }
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
