<?php

namespace App\Models;

use App\Models\Contract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'date', 'status', 'contract_id'];

    public function contracts()
    {
        return $this->hasMany(Contract::class, 'contract_id');
    }

    public function creches()
    {
        return $this->hasOne(Contract::class);
    }

    public function family()
    {
        return $this->hasOne(Family::class);
    }
}
