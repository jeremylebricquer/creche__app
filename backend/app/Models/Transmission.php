<?php

namespace App\Models;

use App\Models\Eat;
use App\Models\Pooh;
use App\Models\Sleep;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transmission extends Model
{
    use HasFactory;

    protected $fillable = ['desc', 'children_id', 'staff_id', 'meal_id', 'sleep_id', 'pooh_id'];

    public function eat()
    {
        return $this->hasOne(Eat::class);
    }

    public function sleep()
    {
        return $this->hasOne(Sleep::class);
    }

    public function pooh()
    {
        return $this->hasOne(Pooh::class);
    }
}
