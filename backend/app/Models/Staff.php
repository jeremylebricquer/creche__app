<?php

namespace App\Models;

use App\Models\User;
use App\Models\Children;
use App\Models\Executive;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Staff extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'firstname', 'planning', 'fonction', 'executive_id'];

    public function executive()
    {
        return $this->hasMany(Executive::class, 'executive_id');
    }

    public function childen()
    {
        return $this->hasMany(Children::class);
    }

    public function family()
    {
        return $this->hasOne(Executive::class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
