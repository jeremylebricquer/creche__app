<?php

namespace App\Models;

use App\Models\Children;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Presence extends Model
{
    use HasFactory;
    protected $fillable = ['status', 'start', 'end', 'children_id'];

    public function childrens()
    {
        return $this->hasOne(Children::class);
    }
}
