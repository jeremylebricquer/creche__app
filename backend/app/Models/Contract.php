<?php

namespace App\Models;

use App\Models\Creche;
use App\Models\Family;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contract extends Model
{
    use HasFactory;

    protected $fillable = ['contractNb', 'contractChildCount'];

    public function families()
    {
        return $this->hasOne(Family::class);
    }

    public function creches()
    {
        return $this->hasOne(Creche::class);
    }
}
