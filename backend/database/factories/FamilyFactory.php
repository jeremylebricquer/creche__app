<?php

namespace Database\Factories;

use App\Models\Family;
use App\Models\Children;
use App\Models\Contract;
use Illuminate\Database\Eloquent\Factories\Factory;

class FamilyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Family::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->lastName(),
            'firstname' => $this->faker->firstName(),
            'contract_id' => $this->faker->unique()->randomElement(Contract::all()),
        ];
    }
}
