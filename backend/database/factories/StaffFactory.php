<?php

namespace Database\Factories;

use App\Models\Executive;
use App\Models\Staff;
use Illuminate\Database\Eloquent\Factories\Factory;

class StaffFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Staff::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->lastName(),
            'firstname' => $this->faker->firstName(),
            'planning' => $this->faker->firstName(),
            'fonction' => $this->faker->firstName(),
            'executive_id' => $this->faker->unique()->randomElement(Executive::all()),
        ];
    }
}
