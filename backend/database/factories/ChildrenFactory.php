<?php

namespace Database\Factories;

use App\Models\Children;
use App\Models\Family;
use App\Models\Presence;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChildrenFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Children::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->lastName(),
            'firstname' => $this->faker->firstName(),
            'photos' => $this->faker->image(),
            'birthday' => $this->faker->dateTimeBetween('2017-01-01', '2020-12-31'),
            'childhood_diseases' => $this->faker->word(),
            'alimetary_regime' => $this->faker->word(),
            'sleep' => $this->faker->time(),
            'weight' => $this->faker->numberBetween(5, 15),
            'allergy' => $this->faker->word(),
            'stools' => $this->faker->word(),
            'activity' => $this->faker->word(),
            'content' => $this->faker->text(),
            'family_id' => Family::all()->random()->id,
        ];
    }
}
