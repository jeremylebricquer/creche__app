<?php

namespace Database\Factories;

use App\Models\Staff;
use App\Models\Family;
use App\Models\Contact;
use App\Models\Executive;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'phone' => $this->faker->phoneNumber(),
            'adress' => $this->faker->address(),
            'email' => $this->faker->email(),
            'family_id' => $this->faker->unique()->randomElement(Family::all()),
        ];
    }
}
