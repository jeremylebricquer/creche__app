<?php

namespace Database\Factories;

use App\Models\Children;
use App\Models\Creche;
use App\Models\Presence;
use Illuminate\Database\Eloquent\Factories\Factory;

class PresenceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Presence::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => $this->faker->boolean(),
            'start' => $this->faker->dateTime(),
            'end' => $this->faker->dateTime(),
            'children_id' => $this->faker->unique()->randomElement(Children::all()),
        ];
    }
}
