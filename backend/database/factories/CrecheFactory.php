<?php

namespace Database\Factories;

use App\Models\Creche;
use App\Models\Contract;
use Illuminate\Database\Eloquent\Factories\Factory;

class CrecheFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Creche::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->lastName(),
            'contract_id' => Contract::all()->random()->id,
        ];
    }
}
