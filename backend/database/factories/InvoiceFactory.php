<?php

namespace Database\Factories;

use App\Models\Invoice;
use App\Models\Contract;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'amount' => $this->faker->numberBetween(50, 500),
            'date' => $this->faker->date(),
            'status' => $this->faker->boolean(),
            'contract_id' => Contract::all()->random()->id,
        ];
    }
}
