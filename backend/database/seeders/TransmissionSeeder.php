<?php

namespace Database\Seeders;

use App\Models\Transmission;
use Illuminate\Database\Seeder;

class TransmissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'desc' => "desc1",
                'children_id' => 1,
                'staff_id' => 1,
                'meal_id' => 3,
                'sleep_id' => 2,
                'pooh_id' => null,

            ],
            [
                'id' => 2,
                'desc' => "desc2",
                'children_id' => 5,
                'staff_id' => 2,
                'meal_id' => 3,
                'sleep_id' => 1,
                'pooh_id' => null,
            ],
            [
                'id' => 3,
                'desc' => "desc3",
                'children_id' => 2,
                'staff_id' => 2,
                'meal_id' => 3,
                'sleep_id' => 3,
                'pooh_id' => null,
            ],
            [
                'id' => 4,
                'desc' => "desc4",
                'children_id' => 6,
                'staff_id' => 3,
                'meal_id' => 3,
                'sleep_id' => 5,
                'pooh_id' => 2,
            ],
            [
                'id' => 5,
                'desc' => "desc5",
                'children_id' => 3,
                'staff_id' => 3,
                'meal_id' => 3,
                'sleep_id' => 6,
                'pooh_id' => 3,
            ],
            [
                'id' => 6,
                'desc' => "desc 6",
                'children_id' => 4,
                'staff_id' => 2,
                'meal_id' => 3,
                'sleep_id' => 4,
                'pooh_id' => 1,
            ],
        ];

        foreach ($items as $item) {
            Transmission::create($item);
        }
    }
}
