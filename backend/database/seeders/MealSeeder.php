<?php

namespace Database\Seeders;

use App\Models\Meal;
use Illuminate\Database\Seeder;

class MealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'type' => 'Midi',
                'desc' => 'gouter les carottes',
                'timestamp' => '2021-05-08 11:31:50',
            ],
            [
                'id' => 2,
                'type' => 'Goûter',
                'desc' => 'Juste une compote',
                'timestamp' => '2021-05-06 15:25:50',
            ],
            [
                'id' => 3,
                'type' => 'Midi',
                'desc' => 'Biberon 150ml',
                'timestamp' => '2021-05-03 12:30:50',
            ],
            [
                'id' => 4,
                'type' => 'Midi',
                'desc' => 'Purée de courgette et veau',
                'timestamp' => '2021-05-02 13:11:50',
            ],
            [
                'id' => 5,
                'type' => 'Goûter',
                'desc' => 'Cracotte avec un laitage et une compote',
                'timestamp' => '2021-05-23 16:31:50',
            ],
            [
                'id' => 6,
                'type' => 'Midi',
                'desc' => 'biberon 250ml',
                'timestamp' => '2021-04-03 12:11:50',
            ],
            [
                'id' => 7,
                'type' => 'Goûter',
                'desc' => 'tartine de pain , chocolat ',
                'timestamp' => '2021-05-04 16:11:50',
            ],
        ];

        foreach ($items as $item) {
            Meal::create($item);
        }
    }
}
