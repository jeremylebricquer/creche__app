<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Staff;
use App\Models\Creche;
use App\Models\Family;
use App\Models\Contact;
use App\Models\Invoice;
use App\Models\Children;
use App\Models\Contract;
use App\Models\Presence;
use App\Models\Executive;
use Illuminate\Database\Seeder;
use Database\Seeders\MealSeeder;
use Database\Seeders\PoohSeeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\SleepSeeder;
use Database\Seeders\StaffSeeder;
use Database\Seeders\ChildrenSeeder;
use Database\Seeders\TransmissionSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        Contract::factory(10)->create();
        Creche::factory(3)->create();
        Family::factory(5)->create();
        $this->call(ChildrenSeeder::class);
        Presence::factory(6)->create();
        Executive::factory(3)->create();
        $this->call(MealSeeder::class);
        $this->call(PoohSeeder::class);
        $this->call(SleepSeeder::class);
        $this->call(StaffSeeder::class);
        $this->call(TransmissionSeeder::class);
        Invoice::factory(5)->create();
        Executive::factory(3)->create();
        Contact::factory(5)->create();
        User::factory(3)->create();
    }
}
