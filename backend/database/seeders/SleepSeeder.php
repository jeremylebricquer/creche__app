<?php

namespace Database\Seeders;

use App\Models\Sleep;
use Illuminate\Database\Seeder;

class SleepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'start' => '2021-05-03 8:11:50',
                'end' => '2021-05-03 9:11:50',
                'desc' => 'courte sieste',

            ],
            [
                'id' => 2,
                'start' => '2021-05-03 13:11:50',
                'end' => '2021-05-03 16:11:50',
                'desc' => 'longue sieste',
            ],
            [
                'id' => 3,
                'start' => '2021-05-03 8:11:50',
                'end' => '2021-05-03 9:11:50',
                'desc' => 'sieste agitée',
            ],
            [
                'id' => 4,
                'start' => '2021-05-03 8:11:50',
                'end' => '2021-05-03 9:11:50',
                'desc' => 'courte sieste',
            ],
            [
                'id' => 5,
                'start' => '2021-05-03 10:11:50',
                'end' => '2021-05-03 11:11:50',
                'desc' => 'courte sieste',
            ],
            [
                'id' => 6,
                'start' => '2021-05-03 8:11:50',
                'end' => '2021-05-03 10:11:50',
                'desc' => 'longue sieste',
            ],
        ];

        foreach ($items as $item) {
            Sleep::create($item);
        }
    }
}
