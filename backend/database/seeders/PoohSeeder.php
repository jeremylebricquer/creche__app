<?php

namespace Database\Seeders;

use App\Models\Pooh;
use Illuminate\Database\Seeder;

class PoohSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'desc' => 'Selle dure',
                'timestamp' => '2021-05-03 9:30:50',
            ],
            [
                'id' => 2,
                'desc' => 'Selle tres liquide ',
                'timestamp' => '2021-06-03 10:11:50',
            ],
            [
                'id' => 3,
                'desc' => 'Selle dure',
                'timestamp' => '2021-05-13 14:31:50',
            ]
        ];

        foreach ($items as $item) {
            Pooh::create($item);
        }
    }
}
