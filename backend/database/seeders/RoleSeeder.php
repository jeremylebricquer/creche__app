<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'type' => 'Admin'],
            ['id' => 2, 'type' => 'Staff'],
            ['id' => 3, 'type' => 'Family'],
        ];

        foreach ($items as $item) {
            Role::create($item);
        }
    }
}
