<?php

namespace Database\Seeders;

use App\Models\Children;
use Illuminate\Database\Seeder;

class ChildrenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'name' => 'Orange',
                'firstname' => 'Clementine',
                'gender' => 'F',
                'birthday' => '2020-08-29',
                'photos' => 'https://archzine.fr/wp-content/uploads/2016/09/id%C3%A9e-tr%C3%A8s-sympa-coiffure-petite-fille-hippie-id%C3%A9e-coiffure-bapteme-e1474977510904.jpg',
                'childhood_diseases' => '',
                'alimetary_regime' => '',
                'weight' => '9',
                'allergy' => '',
                'family_id' => '1'
            ],
            [
                'id' => 2,
                'name' => 'Lb',
                'firstname' => 'Marceau',
                'gender' => 'M',
                'birthday' => '2020-04-03',
                'photos' => 'https://image.freepik.com/photos-gratuite/petit-garcon-mignon-dans-parc-automne_1303-17464.jpg',
                'childhood_diseases' => null,
                'alimetary_regime' => '',
                'weight' => '10',
                'allergy' => null,
                'family_id' => '2'
            ],
            [
                'id' => 3,
                'name' => 'Lb',
                'firstname' => 'Apolline',
                'gender' => 'F',
                'birthday' => '2021-01-02',
                'photos' => 'https://archzine.fr/wp-content/uploads/2016/09/id%C3%A9e-tr%C3%A8s-sympa-coiffure-petite-fille-hippie-id%C3%A9e-coiffure-bapteme-e1474977510904.jpg',
                'childhood_diseases' => null,
                'alimetary_regime' => '',
                'weight' => '4',
                'allergy' => null,
                'family_id' => '2'
            ],
            [
                'id' => 4,
                'name' => 'Brown',
                'firstname' => 'Jude',
                'gender' => 'M',
                'birthday' => '2020-04-03',
                'photos' => 'https://image.freepik.com/photos-gratuite/petit-garcon-mignon-dans-parc-automne_1303-17464.jpg',
                'childhood_diseases' => null,
                'alimetary_regime' => '',
                'weight' => '10',
                'allergy' => null,
                'family_id' => '3'
            ],
            [
                'id' => 5,
                'name' => 'Yellow',
                'firstname' => 'Nicolas',
                'gender' => 'M',
                'birthday' => '2020-04-03',
                'photos' => 'https://image.freepik.com/photos-gratuite/petit-garcon-mignon-dans-parc-automne_1303-17464.jpg',
                'childhood_diseases' => null,
                'alimetary_regime' => '',
                'weight' => '10',
                'allergy' => null,
                'family_id' => '4'
            ],
            [
                'id' => 6,
                'name' => 'Green',
                'firstname' => 'Capucine',
                'gender' => 'F',
                'birthday' => '2020-08-29',
                'photos' => 'https://archzine.fr/wp-content/uploads/2016/09/id%C3%A9e-tr%C3%A8s-sympa-coiffure-petite-fille-hippie-id%C3%A9e-coiffure-bapteme-e1474977510904.jpg',
                'childhood_diseases' => null,
                'alimetary_regime' => '',
                'weight' => '9',
                'allergy' => null,
                'family_id' => '5'
            ],

        ];

        foreach ($items as $item) {
            Children::create($item);
        }
    }
}
