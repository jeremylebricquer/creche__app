<?php

namespace Database\Seeders;

use App\Models\Staff;
use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            [
                'id' => 1,
                'name' => 'Quoniam',
                'firstname' => 'Amandine',
                'planning' => null,
                'fonction' => 'Auxiliaire',
                'executive_id' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Le bricquer',
                'firstname' => 'Quentin',
                'planning' => null,
                'fonction' => 'Educateur',
                'executive_id' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Le bricquer',
                'firstname' => 'Jean-françois',
                'planning' => null,
                'fonction' => 'Boulanger',
                'executive_id' => 1,
            ]
        ];

        foreach ($items as $item) {
            Staff::create($item);
        }
    }
}
