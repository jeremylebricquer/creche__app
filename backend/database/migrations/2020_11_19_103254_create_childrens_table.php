<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('childrens', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('firstname');
            $table->string('gender')->nullable();;
            $table->date('birthday');
            $table->binary('photos')->nullable();;
            $table->string('childhood_diseases')->nullable();
            $table->string('alimetary_regime')->nullable();
            $table->integer('weight')->nullable();
            $table->string('allergy')->nullable();
            $table->foreignId('family_id')->constrained('families')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('childrens');
    }
}
