<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\MealController;
use App\Http\Controllers\Api\PoohController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\SleepController;
use App\Http\Controllers\Api\StaffController;
use App\Http\Controllers\Api\CrecheController;
use App\Http\Controllers\Api\FamilyController;
use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\ChildrenController;
use App\Http\Controllers\Api\ContractController;
use App\Http\Controllers\API\PresenceController;
use App\Http\Controllers\Api\ExecutiveController;
use App\Http\Controllers\Api\TransmissionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Auth routes
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);


// Route for classic role to resources
/* Route::middleware('auth:api')->group(function () {
    Route::apiResource('children', ChildrenController::class);
    Route::apiResource('family', FamilyController::class);
    Route::apiResource('contract', ContractController::class);
    Route::apiResource('creche', CrecheController::class);
    Route::apiResource('invoice', InvoiceController::class);
    Route::apiResource('executive', ExecutiveController::class);
    Route::apiResource('staff', StaffController::class);
    Route::apiResource('contact', ContactController::class);
    Route::apiResource('role', RoleController::class);
    Route::apiResource('user', UserController::class);
}); */

// Route without auth
Route::apiResource('children', ChildrenController::class);
Route::apiResource('family', FamilyController::class);
Route::apiResource('contract', ContractController::class);
Route::apiResource('creche', CrecheController::class);
Route::apiResource('invoice', InvoiceController::class);
Route::apiResource('executive', ExecutiveController::class);
Route::apiResource('contact', ContactController::class);
Route::apiResource('role', RoleController::class);
Route::apiResource('user', UserController::class);
Route::apiResource('staff', StaffController::class);
Route::apiResource('presence', PresenceController::class);
Route::apiResource('meal', MealController::class);
Route::apiResource('sleep', SleepController::class);
Route::apiResource('pooh', PoohController::class);
Route::apiResource('transmission', TransmissionController::class);
